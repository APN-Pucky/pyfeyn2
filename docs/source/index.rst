.. pyfeyn2 documentation master file, created by
   sphinx-quickstart on Sun Nov 27 01:18:14 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.




Welcome to pyfeyn2's documentation!
===================================

.. include:: ../../README.md
   :parser: myst_parser.sphinx_


.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. toctree::
   :glob:
   :hidden:
   :caption: Versions:
   :maxdepth: 3

   Stable <https://apn-pucky.github.io/pyfeyn2/>
   Dev <https://apn-pucky.github.io/pyfeyn2/test/>

.. toctree::
   :glob:
   :maxdepth: 3
   :caption: FeynML:

   feynml/index.rst

.. toctree::
   :glob:
   :maxdepth: 3
   :caption: Examples:

   examples/*

.. toctree::
   :glob:
   :maxdepth: 3
   :caption: Module:

   autoapi/index.rst




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
