.. _style:

Style
======
| Format: String
| Valid on: :ref:`leg`, :ref:`propagator`

A CSS-like semi-colon separated list of style properties. The following properties are supported per implementation:

.. include:: ../../shared/style_tab.rst


.. toctree::
   :glob:
   :maxdepth: 3
   :hidden:

   style/*
