.. _arrow-pos:

arrow-pos
=========

| Valid on: :ref:`leg`, :ref:`propagator`
| Format: Double
| Default: ?

.. include:: ../../../shared/style/arrow-pos.rst
