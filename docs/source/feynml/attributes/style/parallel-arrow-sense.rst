.. _parallel-arrow-sense:

parallel-arrow-sense
====================

| Valid on: :ref:`leg`, :ref:`propagator`
| Format: Integer
| Default: 1

.. include:: ../../../shared/style/parallel-arrow-sense.rst
