.. _parallel-arrow-displace:

parallel-arrow-displace
=======================

| Valid on: :ref:`leg`, :ref:`propagator`
| Format: Double
| Default: ?

.. include:: ../../../shared/style/parallel-arrow-displace.rst
