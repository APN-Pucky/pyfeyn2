.. _pdgid:

PDGID
=====
| Format: Integer
| Elements: :ref:`leg`, :ref:`propagator`
